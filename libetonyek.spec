%global apiversion 0.1

Name: libetonyek
Version: 0.1.10
Release: 2
Summary: A library for import of Apple iWork documents

License: MPL-2.0
URL: http://wiki.documentfoundation.org/DLP/Libraries/libetonyek
Source: http://dev-www.libreoffice.org/src/%{name}/%{name}-%{version}.tar.xz

BuildRequires: boost-devel doxygen gcc-c++ glm-devel gperf help2man make pkgconfig(cppunit) pkgconfig(liblangtag)
BuildRequires: pkgconfig(librevenge-0.0) pkgconfig(librevenge-generators-0.0) pkgconfig(librevenge-stream-0.0) 
BuildRequires: libxml2-devel pkgconfig(mdds-2.1) 
# FIXME: zlib will conflict with zlib-ng, but zlib-ng is not available in mainline.
# BuildRequires: pkgconfig(zlib)

%description
%{name} is library for import of Apple iWork documents. It supports
documents created by any version of Keynote, Pages or Numbers.

%package devel
Summary: Development files for %{name}
Requires: %{name}%{?_isa} = %{version}-%{release}

%description devel
The %{name}-devel package contains libraries and header files for
developing applications that use %{name}.

%package_help

%package tools
Summary: Tools to transform Apple iWork documents into other formats
Requires: %{name}%{?_isa} = %{version}-%{release}

%description tools
Tools to transform Apple iWork documents into other formats. Currently
supported: CSV, HTML, SVG, text, and raw.

%prep
%autosetup -p1

%build
%configure --disable-silent-rules --disable-static --disable-werror --with-mdds=2.1
sed -i \
    -e 's|^hardcode_libdir_flag_spec=.*|hardcode_libdir_flag_spec=""|g' \
    -e 's|^runpath_var=LD_RUN_PATH|runpath_var=DIE_RPATH_DIE|g' \
    libtool
%make_build

%install
%make_install
rm -f %{buildroot}/%{_libdir}/*.la
# we install API docs directly from build
rm -rf %{buildroot}/%{_docdir}/%{name}

# generate and install man pages
export LD_LIBRARY_PATH=%{buildroot}/%{_libdir}${LD_LIBRARY_PATH:+:${LD_LIBRARY_PATH}}
for tool in key2raw key2text key2xhtml numbers2csv numbers2raw numbers2text pages2html pages2raw pages2text; do
    help2man -N -S '%{name} %{version}' -o ${tool}.1 %{buildroot}%{_bindir}/${tool}
done
install -m 0755 -d %{buildroot}/%{_mandir}/man1
install -m 0644 key2*.1 numbers2*.1 pages2*.1 %{buildroot}/%{_mandir}/man1

%ldconfig_scriptlets

%check
export LD_LIBRARY_PATH=%{buildroot}/%{_libdir}${LD_LIBRARY_PATH:+:${LD_LIBRARY_PATH}}
if ! %make_build check; then
    cat src/test/*.log
    exit 1
fi

%files
%doc AUTHORS FEATURES NEWS README
%license COPYING
%{_libdir}/%{name}-%{apiversion}.so.*

%files devel
%doc ChangeLog
%{_includedir}/%{name}-%{apiversion}
%{_libdir}/%{name}-%{apiversion}.so
%{_libdir}/pkgconfig/%{name}-%{apiversion}.pc

%files help
%license COPYING
%doc docs/doxygen/html
%{_mandir}/man1/key2raw.1*
%{_mandir}/man1/key2text.1*
%{_mandir}/man1/key2xhtml.1*
%{_mandir}/man1/numbers2csv.1*
%{_mandir}/man1/numbers2raw.1*
%{_mandir}/man1/numbers2text.1*
%{_mandir}/man1/pages2html.1*
%{_mandir}/man1/pages2raw.1*
%{_mandir}/man1/pages2text.1*

%files tools
%{_bindir}/key2raw
%{_bindir}/key2text
%{_bindir}/key2xhtml
%{_bindir}/numbers2csv
%{_bindir}/numbers2raw
%{_bindir}/numbers2text
%{_bindir}/pages2html
%{_bindir}/pages2raw
%{_bindir}/pages2text

%changelog
* Sat Apr 20 2024 shafeipaozi <sunbo.oerv@isrc.iscas.ac.cn> - 0.1.10-2
- fix build with newer mdds

* Wed Sep 06 2023 Darssin <2020303249@mail.nwpu.edu.cn> - 0.1.10-1
- Package init
